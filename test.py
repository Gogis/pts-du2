import unittest
import random
import main

class CloveceTest(unittest.TestCase):

    def setUp(self):
        main.game_init() 

    def test_move(self):
        with open('tests/move.in') as fin:
            in_lines = fin.readlines()

        with open('tests/move.out') as fout:
            out_lines = fout.readlines()

        act_player = 0
        for in_line, act_pos in zip(in_lines, out_lines):
            figure, dice = map(int, in_line.split())

            old_state = main.game_state
            main.tah(dice, figure)

            self.assertEqual(int(act_pos), main.game_state[0][act_player][figure])

            act_player = (act_player + 1) % main.PLAYERS

    def test_next_to_move(self):
        with open('tests/move.in') as fin:
            in_lines = fin.readlines()

        with open('tests/move_next.out') as fout:
            out_lines = fout.readlines()

        for in_line, next_move in zip(in_lines, out_lines):
            figure, dice = map(int, in_line.split())

            self.assertEqual(int(next_move), main.game_state[1])
            main.tah(dice, figure)

            
if __name__ == "__main__":
    unittest.main()

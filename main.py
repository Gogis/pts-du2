from random import randint

BOARD_SIZE = 40
# 4 players is the default
PLAYERS = 4
game_state = None

def dice_roll():
    return randint(1,6)


def game_init():
    """Vratim pociatocny stav hry ako (pozicie_figurok, 
        kto_je_na_tahu, cislo_kola)"""
    
    global game_state
    players = [(0, 0, 0, 0) for x in range(PLAYERS)]
    game_state = (players, 0)


def get_figure():
    while True:
        try:
            cislo = int(input(">> Zadaj cislo figurky "
                "s ktorou chces pohnut: \n"))
            if cislo >= 0 and cislo < 4: break
            print(">> ERROR: Neplatne cislo figurky! Zadaj este raz: ")
        except ValueError:
            print(">> ERROR: Ooops! Neplatny vstup.")
        except EOFError:
            print(">> Najdene EOF. Koniec hry.")
            exit(0)

    return cislo


def tah(kolko, ktorou_figurkou):
    """ Urobim deepcopy aktualneho stavu a vratim ju zmenenu o novy tah """
    global game_state
    kto = game_state[1]

    players = []
    for x in range(len(game_state[0])):
        if x == kto:
            lst = list(game_state[0][x])
            lst[ktorou_figurkou] += kolko
            lst[ktorou_figurkou] %= BOARD_SIZE # hraci plan ma BOARD_SIZE policok
            players.append(tuple(lst))
        else:
            players.append(game_state[0][x])
    
    dalsi_na_tahu = (game_state[1] + 1) % len(game_state[0])
    # priradim do referencie novy stav
    game_state = (players, dalsi_na_tahu)


def print_state():
    cislo_hraca = 0
    for hrac in game_state[0]:
        print("Hrac", cislo_hraca, "ma figurky na poziciach: ", end="")
        print(*enumerate(hrac))
        cislo_hraca += 1


def main():
    game_init()

    while True:
        print_state()
        
        roll = dice_roll()
        figurka = get_figure()
        print("Hrac", game_state[1], "hodil cislo", roll)
        tah(roll, figurka)

if __name__ == '__main__':
    main()


## Druha domaca uloha z PTS

# Zadanie
Domácu úlohu odovzdajte ako GITovský repozitár e-mailom na relatko@gmail.com. Do subjectu napiste PTS DU2. Nezabudnite napisat svoje meno. Termín na odovzdanie úlohy je 4.4.2017 23:00

Naprogramujte terminálové "Človeče nehnevaj sa!". Môžte si upraviť pravidlá. Odporúčané úpravy (zjednoduženia): po hodení 6-ky hr=ač nehádže ešte raz, po obkrúžení kolečka panáčikovia nejdu do "domčeka", ale sa točia daľej (teda domček ani neexistuje). Program taktiež nemusí byť úplne user friendly.
Aktuálny stav hry má byť uložený ako jedna imutable premenná, a referencia na tento stav má byť v podstate jedinou "verejnou" mutovatelnou premennou v programe. To samozrejme neznamená, že v kóde nemajú byť mutable premenné, takéto premenné však majú existovať iba v lokálnom scope.
Súčasťou odovzdaného repozitára má byť základná používateľská dokumentácia, stručný popis designu a aspoň základné testovacie programy.

# Moje (slightly upravene) pravidla
Hru hraju 4 hraci. Hraci plan ma 40 policok. Kazdy hrac ma 4 figurky, ktore su na zaciatku hry vsetky postavene na policku 0.
Nasledne sa hraci striedaju v hode kockou. Hrac si po kazdom hode vyberie, ktorou figurkou sa o dane cislo posunie. Na jednom policku moze stat hocikolko figurok. Figurky sa na hracom plane tocia dokola, hra nikdy nekonci.

# Organizacia kodu
Stav hry je reprezentovany ako tuple (players, kto_na_tahu), kde 'players' je list reprezentujuci jednotlivych hracov.
Kazdy hrac ma v liste 'players' tuple so styrmi hodnotami reprezentujucimi pozicie jeho jednotlivych
figurok. Vsetky figurky vsetkych hracov sa na zaciatku hry inicializuju na 0 vo funkcii 'init'.

Funkcia 'dice_roll' vrati nahodne cislo z rozsahu <1-6>, realne teda simuluje hody kockou.

Funkcia 'get_figure' nacita zo standardneho vstupu cislo figurky, ktorou chce aktualny hrac pohnut.

Funkcia 'tah' urobi deepcopy aktualneho stavu s tym, ze vytvori nove tuple pre hraca, ktory prave
urobil tah. 

Funkcia 'print_state' vypise aktualny stav hry - pozicie jednotlivych figurok vsetkych 4 hracov.
